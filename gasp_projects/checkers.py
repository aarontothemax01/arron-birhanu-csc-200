#Player vs Player checkers game
#By Richard Martinez and Arron Birhanu

#gasp modules necessary to develop this game
from gasp import games
from gasp import boards
from gasp import color


# git reset --hard HEAD (resets to previous commit)

# global variables, most of which define the board
BOX_SIZE = 40
MARGIN = 60
BOARD_SIZE = 8
WHITE = color.WHITE
BLACK = color.BLACK
BOARD_RED = color.RED
BOARD_BLACK = color.BLACK
COUNTER_SIZE = BOX_SIZE/2 - 2
NEXT_PLAYER = { WHITE : BLACK,
BLACK : WHITE }

class checkersGrid(boards.GameCell): #grid cells established on the board
    def __init__(self, board, i, j): #initializing gamecell attributes
        if i+j & 1 == 0:
            box_color = BOARD_RED
        else:
            box_color = BOARD_BLACK
        self.init_gamecell(board, i, j, fill_color = box_color)
        self.counter = None

    def center_pos(self): #translating central screen coordinates to grid coordinates of each counter we wish to move
        (x, y) = self.board.cell_to_coords(self.grid_x, self.grid_y)
        return x + BOX_SIZE/2, y + BOX_SIZE/2

    def new_counter(self, player): #creating a new counter and its starting position
        self.counter = Counter(self.board, self.screen_x + BOX_SIZE/2,
        self.screen_y + BOX_SIZE/2, player)

    def get_counter_color(self): #returns the color, or more importantly which player, that owns the counter
        if self.counter is None:
            return

        return self.counter.get_color()
    
    def move_counter(self, i, j): #responsible for moving the counter

        if not self.board.on_board(i, j): #can't leave board
            return
        
        cell = self.board.grid[i][j]
        
        if self.counter is None or cell.counter is not None:
            return
        
        new_x, new_y = cell.center_pos()      
        self.counter.move_to(new_x, new_y)
        cell.counter = self.counter
        self.counter = None

        return True

    def can_take(self, key): #responsible for taking counters
        counter_color = self.get_counter_color()

        if counter_color == WHITE:
            di, dj = self.screen.white_move[key]
        elif counter_color == BLACK:
            di, dj = self.screen.black_move[key]
        else:
            return
        
        if self.counter.is_crowned: #in the case of crowned counters taking a non crowned counter
            if self.screen.is_pressed(games.K_w):
                di, dj = self.screen.black_move[key]
            elif self.screen.is_pressed(games.K_s):
                di, dj = self.screen.white_move[key]
                    
        i, j = self.grid_x + di, self.grid_y + dj
        if self.board.on_board(i, j):      
            return self.board.grid[i][j].get_counter_color() == NEXT_PLAYER[counter_color]

    def remove_counter(self): #remove counter from board
        if self.counter is None:
            return
        self.counter.destroy()
        self.counter = None
        
class checkersBoard(boards.SingleBoard): #the board in which two players operates pieces
    def __init__(self, box_size, margin, n_cols, n_rows): #initializing singleboard's attributes
        self.init_singleboard((MARGIN, MARGIN), n_cols, n_rows, box_size,
                              cursor_color=color.YELLOW)
        
        self.game_over = 0
        self.draw_all_outlines()
        self.enable_cursor(0, 0)
        self.set_background_color(color.DARKGREEN)
        self.white_move = {
            games.K_a: (-1, 1),
            games.K_d: (1, 1),
        }
        self.black_move = {
            games.K_a: (-1, -1),
            games.K_d: (1, -1),
        }
        
        self.grid[0][0].new_counter(WHITE) #grid location and creation of each counter, will add for loops for future update
        self.grid[2][0].new_counter(WHITE)
        self.grid[4][0].new_counter(WHITE)
        self.grid[6][0].new_counter(WHITE)
        self.grid[1][1].new_counter(WHITE)
        self.grid[3][1].new_counter(WHITE)
        self.grid[5][1].new_counter(WHITE)
        self.grid[7][1].new_counter(WHITE)
        self.grid[0][2].new_counter(WHITE)
        self.grid[2][2].new_counter(WHITE)
        self.grid[4][2].new_counter(WHITE)
        self.grid[6][2].new_counter(WHITE)
        self.grid[1][5].new_counter(BLACK)
        self.grid[3][5].new_counter(BLACK)
        self.grid[5][5].new_counter(BLACK)
        self.grid[7][5].new_counter(BLACK)
        self.grid[0][6].new_counter(BLACK)
        self.grid[2][6].new_counter(BLACK)
        self.grid[4][6].new_counter(BLACK)
        self.grid[6][6].new_counter(BLACK)
        self.grid[1][7].new_counter(BLACK)
        self.grid[3][7].new_counter(BLACK)
        self.grid[5][7].new_counter(BLACK)
        self.grid[7][7].new_counter(BLACK)

        self.current_turn = WHITE
        self.num_whites = 12
        self.num_blacks = 12
        
        x = self.get_width()/2
        y = MARGIN/2
        self.text = games.Text(self, x, y, text="White's Turn", size=35, color=WHITE)
        
    def new_gamecell(self, i, j): #initialize creation of squares on the board
        return checkersGrid(self, i, j)

        #grid is a list within the Gameboard class designed to
        #host pieces across coordinates shown as a list

    def toggle_turn(self): #the player's turn
        self.current_turn = NEXT_PLAYER[self.current_turn]
        if self.current_turn == WHITE:
            self.text.set_text("White's Turn")
        elif self.current_turn == BLACK:
            self.text.set_text("Black's Turn")
    
    def keypress(self, key): #manages the keys pressed
        if self.game_over:
            return
        
        self.handle_cursor(key)

        
        
        if key in self.white_move.keys():
            counter_color = self.cursor.get_counter_color()

            if counter_color == WHITE and self.current_turn == WHITE:
                di, dj = self.white_move[key]
            elif counter_color == BLACK and self.current_turn == BLACK:
                di, dj = self.black_move[key]
            else:
                return


            if self.cursor.counter.is_crowned: #crowned player key binds
                if self.is_pressed(games.K_w):
                    di, dj = self.black_move[key]
                elif self.is_pressed(games.K_s):
                    di, dj = self.white_move[key]
            
            i, j = self.cursor.grid_x + di, self.cursor.grid_y + dj
            
            if self.cursor.can_take(key):
                if self.grid[i+di][j+dj].get_counter_color():
                    return
                self.grid[i][j].remove_counter()
                if counter_color == WHITE:
                    self.num_blacks -= 1
                elif counter_color == BLACK:
                    self.num_whites -= 1
                i += di
                j += dj

            
            if counter_color == WHITE and j == 7: #where counter is crowned and what happens to counter
                self.cursor.counter.add_crown()
            elif counter_color == BLACK and j == 0:
                self.cursor.counter.add_crown()
                
            if self.cursor.move_counter(i, j):         
                if self.on_board(i, j):
                    self.toggle_turn()        
            
    def tick(self): #keeps track of each move, determines when end game is
        if self.game_over:
            return
        
        if self.num_blacks <= 0:
            self.end_game(WHITE)
        elif self.num_whites <= 0:
            self.end_game(BLACK)
        
    def end_game(self, winner): #kills game, determines who wins
        if winner == WHITE:
            self.text.set_text("White Wins!")
        elif winner == BLACK:
            self.text.set_text("Black Wins!")
        self.disable_cursor()
        self.game_over = 1
                
         
class Counter(boards.Container): #creation of each counter (object)
    def __init__(self, board, x, y, player): #initializing container's attributes
        self.init_container(["crown_circle", "player_circle"]) #the visual order in which the crown and counter appear in
        self.crown_circle = games.Circle(board, x, y, radius=COUNTER_SIZE/4,
                                          color=color.BLUE)
        self.player_circle = games.Circle(board, x, y, radius=COUNTER_SIZE,
                                          color=player)
        self.is_crowned = False

    def get_color(self):    #color of counter called
        return self.player_circle.get_color()

    def add_crown(self): #attributes of the crown
        self.is_crowned = True
        self.crown_circle.raise_object()
        
checkers = checkersBoard(BOX_SIZE, MARGIN, BOARD_SIZE, BOARD_SIZE)
checkers.mainloop()
