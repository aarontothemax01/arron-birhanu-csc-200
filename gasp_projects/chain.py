from gasp import games
from gasp import boards
from gasp import color
from random import randint
import gasp
print(gasp.__file__)

BOX_SIZE = 40
MARGIN = 60
COUNTER_SIZE = BOX_SIZE/2 - 3
NUMBER_SIZE = 24
TEXT_SIZE = 18

PLAYER1 = color.WHITE
PLAYER2 = color.BLACK
EMPTY_COLOR = color.LIGHTGRAY
NEXT_PLAYER = { PLAYER1 : PLAYER2, PLAYER2 : PLAYER1 }

COLOR = { PLAYER1 : "White", PLAYER2 : "Black" }

def evenColumns(box):
  return box.grid_x % 2 == 0

class Atom(boards.Container, boards.GameCell):
  def __init__(self, board, i, j):
    self.init_container(['counter'])
    self.init_gamecell(board, i, j)

    if self.counter != None and self.counter.get_colour() != player:
             return 0
    
  def add_counter(self, player):
           if self.counter == None:
             self.counter = Counter(self.board, self.screen_x + BOX_SIZE/2,
                                    self.screen_y + BOX_SIZE/2, player)
    
class ChainReaction(boards.SingleBoard):
  def __init__(self, n_cols, n_rows):
    self.current_player = PLAYER1
    self.check = 0
    self.game_over = 0
    self.num_counters =  "PLAYER1 : 0,                           PLAYER2 : 0"
    self.init_singleboard((MARGIN, MARGIN), n_cols, n_rows, BOX_SIZE)
    self.create_neighbors(orthogonal_only = 1)
    self.enable_cursor(0, 0)
    self.key_movements = {
      games.K_w: (0,-1),
      games.K_a: (-1,0),
      games.K_s: (0,1),
      games.K_d: (1,0),
      games.K_e: (1,-1)
      }
    self.set_background_color(EMPTY_COLOR)
    
  def new_gamecell(self, i, j):
    return Atom(self, i, j)
      
  def keypress(self, key):
    if self.game_over:
      return
    
    self.handle_cursor(key)
    
    if key == games.K_SPACE:
       print("hello world")
    
    if key == games.K_RETURN:
             self.cursor.add_counter(self.current_player)

  def tick(self):
    self.limit_outlines(fn=evenColumns, true_color=color.BLUE, false_color=EMPTY_COLOR)
  
class Counter(boards.Container, games.Circle):
  def __init__(self, board, x, y, player):
    self.height = 1
    self.init_container(['number'])
    self.init_circle(board, x, y, COUNTER_SIZE, player, filled=True)
    self.number = games.Text(board, x, y, "1", NUMBER_SIZE, colour.yellow)

  def higher(self):
    self.height = self.height + 1
    self.number.set_text(str(self.height))

  
chain_reaction = ChainReaction(8, 8)
chain_reaction.mainloop()
