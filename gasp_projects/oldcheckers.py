from gasp import games
from gasp import boards
from gasp import color

BOX_SIZE = 40
MARGIN = 60
COUNTER_SIZE = BOX_SIZE/2 - 2
CROWN_SIZE = BOX_SIZE/10
TEXT_SIZE = 18
BOARD_SIZE = 8
CROWN_COLOR = color.BLUE
WHITE = color.WHITE
BLACK = color.BLACK
BOARD_RED = color.RED
BOARD_BLACK = color.BLACK
SELECTED = color.YELLOW

NAME = { WHITE : "White",
         BLACK : "Black" }


NEXT_PLAYER = {WHITE : BLACK,
               BLACK : WHITE}

DIRECTIONS = { BLACK : (boards.UP_LEFT, boards.UP_RIGHT),
               WHITE : (boards.DOWN_LEFT, boards.DOWN_RIGHT) }


ALL_DIRECTIONS = [ boards.UP_LEFT, boards.UP_RIGHT,
boards.DOWN_LEFT, boards.DOWN_RIGHT ]


#if row+col=even then box=black and if row+col=odd then box=white

def check_for_takes(box):
  if box.can_take():
    box.add_to_take_list()

class checkersGrid(boards.GameCell): #DraughtsBox
    def __init__(self, board, i, j):
        if i+j & 1 == 0:
            box_color = BOARD_RED
        else:
            box_color = BOARD_BLACK
        self.init_gamecell(board, i, j, fill_color = box_color)
        
        

    
    
    def new_counter(self, player):
        self.counter = Counter(self.board, self.screen_x + BOX_SIZE/2,
        self.screen_y + BOX_SIZE/2, player)

      
    def try_move(self, destination):
        
      if destination.counter != None:
        return 0

      dirs = self.counter.viable_directions()
      for d in dirs:
        if destination is self.direction[d]:
          self.counter.move_to(destination.screen_x + BOX_SIZE/2, destination.screen_y + BOX_SIZE/2)
          self.counter.raise_object()
          destination.counter = self.counter
          self.counter = None
          self.deselect()
          return 1
      return 0


    def try_take(self, destination, player):
    ##The first part looks just like try_move
      if destination.counter != None:
        return 0
      dirs = self.counter.viable_directions()
      for d in dirs:
      ##Look for destination two steps away
        middle = self.direction[d]
        if middle != None and middle.direction[d] is destination:
        ##Complain if there’s no counter to take
          ##or if it’s one of the player’s own
          if (middle.counter is None or middle.counter.is_player(player)):
            return 0

          self.counter.move_to(destination.screen_x + BOX_SIZE/2, destination.screen_y + BOX_SIZE/2)
          self.counter.raise_object()
          destination.counter = self.counter
          self.counter = None
          self.deselect()
          ##Remove the counter that’s been taken
          middle.counter.destroy()
          middle.counter = None
          if destination.can_take():
            self.board.force(destination)
          return 1
          destination.counter.attempt_crowning(destination.grid_y)
      return 0
    
    def can_take(self):
      player = self.board.current_player

      if self.counter == None or not self.counter.is_player(player):
        return 0
      dirs = self.counter.viable_directions()
      for d in dirs:


        middle = self.direction[d]
        if (middle != None and middle.counter != None and
            not middle.counter.is_player(player)):

          destination = middle.direction[d]
          if destination != None and destination.counter == None:

            return 1
        destination.counter.attempt_crowning(destination.grid_y)
      return 0

    def add_to_take_list(self):
        self.board.take_list.append(self)
    
class checkersBoard(boards.SingleBoard): #DraughtsBoard
    def __init__(self, box_size, margin, n_cols, n_rows):
        self.init_singleboard((margin, margin), n_cols, n_rows, box_size,
                               cursor_color=color.YELLOW)
        self.create_directions()
        self.game_over = 0
        self.current_player = WHITE
        self.enable_cursor(0, 0)
        self.set_background_color(color.LIGHTSLATEGRAY)
        self.take_list = []
        
        self.counters_left = {WHITE: 12, BLACK: 12}
        self.draw_all_outlines()
        self.grid[0][0].new_counter(WHITE)
        self.grid[2][0].new_counter(WHITE)
        self.grid[4][0].new_counter(WHITE)
        self.grid[6][0].new_counter(WHITE)
        self.grid[1][1].new_counter(WHITE)
        self.grid[3][1].new_counter(WHITE)
        self.grid[5][1].new_counter(WHITE)
        self.grid[7][1].new_counter(WHITE)
        self.grid[0][2].new_counter(WHITE)
        self.grid[2][2].new_counter(WHITE)
        self.grid[4][2].new_counter(WHITE)
        self.grid[6][2].new_counter(WHITE)
        self.grid[1][5].new_counter(BLACK)
        self.grid[3][5].new_counter(BLACK)
        self.grid[5][5].new_counter(BLACK)
        self.grid[7][5].new_counter(BLACK)
        self.grid[0][6].new_counter(BLACK)
        self.grid[2][6].new_counter(BLACK)
        self.grid[4][6].new_counter(BLACK)
        self.grid[6][6].new_counter(BLACK)
        self.grid[1][7].new_counter(BLACK)
        self.grid[3][7].new_counter(BLACK)
        self.grid[5][7].new_counter(BLACK)
        self.grid[7][7].new_counter(BLACK)

        tx = self.get_width()/2 ##text x pos
        ty = MARGIN/2 ##text y pos
        
        self.status = games.Text(self, tx, ty, "White’s turn",
                                 TEXT_SIZE, color.WHITE) ##text imprinted   
    def force(self, box):
        self.forcing = 1
        self.selection = box.select(self.current_player)
        self.take_list = [ box ]
        
    def new_gamecell(self, i, j): #initialize creation of squares on the board
        return checkersGrid(self, i, j)

    def keypress(self, key):
        self.handle_cursor(key)
        if self.game_over:
            return
      
        if key == games.K_SPACE:
            self.selection()
            self.action
            if key == games.K_SPACE:
              self.move_cursor(i, j)
              i, j = self.coords_to_cell(i, j)
            return
    
        

    def action(self):
      if self.selection is None:
        return
      if self.take_list == [] or self.cursor in self.take_list:
        self.selection = self.cursor.select(self.current_player)
      elif self.selection is self.cursor:
        self.cursor.deselect()
        self.selection = None

      elif self.take_list == [] and self.selection.try_move(self.cursor):
        self.end_turn()

      elif (self.take_list != [] and self.selection.try_make(self.cursor, self.current_player)):
        self.end_turn()


      next = NEXT_PLAYER[self.current_player]
      self.counters_left[next] = self.counters_left[next] - 1
      

    def end_turn(self):
        next = NEXT_PLAYER[self.current_player]
        if self.counters_left[next] == 0:
          self.end_game()
          return
        self.current_player = next
        self.selection = None

        self.status.set_text(NAME[self.current_player] + "’s turn")
        self.take_list = []
        self.map_grid(check_for_takes)
        if not self.forcing or self.selection is not self.cursor:
          self.end_turn()

          
        self.forcing = (len(self.take_list) == 1)
        if self.forcing:
          self.selection = self.take_list[0].select(self.current_player)

    def select(self, player):
      if self.counter is None or not self.counter.is_player(player):
        return None
      self.set_color(SELECTED)
      return self

      self.forcing = 0
    
class Counter(games.Circle):
    def __init__(self, board, x, y, player):
        self.init_circle(board, x, y, COUNTER_SIZE, player)
        
########### WHILE LOOP INCOMING: FOR ALL PIECES ON BOARD ######################
        
##############################################################################        

    def viable_directions(self):
      if self.crown != None:
        return ALL_DIRECTIONS
      return DIRECTIONS[self.get_colour()]

    def attempt_crowning(self, row):
      if self.crown != None:
        return
      if ((self.is_player(WHITE) and row == 7) or
          (self.is_player(BLACK) and row == 0)):
        self.crown = games.Circle(self.screen, self.xpos(), self.ypos(), CROWN_SIZE, CROWN_COLOR)

    def end_game(self):
      self.game_over = 1
      self.disable_cursor()
      self.status.set_text(NAME[self.current_player] + " wins the game!")
    
checkers = checkersBoard(BOX_SIZE, MARGIN, BOARD_SIZE, BOARD_SIZE)
checkers.mainloop()

#The mainloop method handles moving your objects around and updating the screen.
#mainloop does not return until the player quits the game
